package com.example.tamas.evaapplication;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.renderscript.ScriptGroup;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tamas.evaapplication.KomenEten.KomenEtenStartActivity;
import com.example.tamas.evaapplication.validation.InputValidatorHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity{

    @Bind(R.id.imgLogo)
    ImageView logo;
    @Bind(R.id.txtUsername)
    EditText emailAdress;
    @Bind(R.id.txtPassword)
    EditText password;
    @Bind(R.id.chkRemember)
    CheckBox rememberMe;
    @Bind(R.id.tvRegister)
    TextView register;
    @Bind(R.id.btnLogin)
    Button loginButton;
    @Bind(R.id.lblWachtwoord)
    TextView lblWachtWoord;
    @Bind(R.id.lblEmail)
    TextView lblEmail;


    Animation animationFadeIn;
    UserLocalStore userLocalStore;
    boolean allowSave = true;
    InputValidatorHelper validator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        userLocalStore = new UserLocalStore(this);
        validator = new InputValidatorHelper();
        //initializeValidation();
        AssetManager am = getApplicationContext().getAssets();
        //animationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);

        //logo.startAnimation(animationFadeIn);
    }

    @OnClick(R.id.btnLogin)
    public void login(View v) {
        InputValidatorHelper validator = new InputValidatorHelper();
        if(!validator.isValidPassword(password.getText().toString(), false)){
            //MOET NOG IN DE STRING.XML VOOR VERTALING
            password.setError("Je hebt geen geldig wachtwoord ingevoerd");
            allowSave = false;
        }
        if (!validator.isValidEmail(emailAdress.getText().toString())) {
            //MOET NOG IN DE STRING.XML VOOR VERTALING
            emailAdress.setError("Je moet een geldig emailadres invoeren");
            allowSave = false;
        }
        if(allowSave){
            User user = new User();
            user.setEmail(emailAdress.getText().toString());
            user.setPassword(password.getText().toString());
            //EERST CONTROLE NAAR DATABANK OF USER AL BESTAAT
            //ZOJA
            if(rememberMe.isChecked()){
                userLocalStore.storeUserData(user);

            }
            userLocalStore.setUserLoggedIn(true);
            startActivity(new Intent(this, KomenEtenStartActivity.class));
            //ZONEE
        }

    }
    @OnClick(R.id.tvRegister)
    public void register(View v){
        startActivity(new Intent(this, RegistrationPart1Activity.class));
    }
    /*public void initializeValidation(){

        emailAdress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!validator.isValidEmail(emailAdress.getText().toString())) {
                    //MOET NOG IN DE STRING.XML VOOR VERTALING
                    emailAdress.setText("Je moet een geldig emailadres invoeren");
                    allowSave = false;
                }

            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!validator.isValidPassword(password.getText().toString(), false)){
                    //MOET NOG IN DE STRING.XML VOOR VERTALING
                    password.setError("Je hebt geen geldig wachtwoord ingevoerd");
                    allowSave = false;
                }
            }
        });
    } */
    //VOOR DE LOGOUT BUTTON
    //userLocalStore.clearUserData();
    //userLocalStore.setUserLoggedIn(false);
}
