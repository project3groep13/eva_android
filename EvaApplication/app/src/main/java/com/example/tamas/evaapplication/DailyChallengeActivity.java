package com.example.tamas.evaapplication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.tamas.evaapplication.KomenEten.KomenEtenStartActivity;

import org.w3c.dom.Text;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DailyChallengeActivity extends AppCompatActivity {
    TextView datumVandaag,challengedescr,progress;
    RadioGroup radioGroup;
    RadioButton option1,option2,option3;
    Button bVoltooid;
    @Bind(R.id.bVolgende)
    Button volgende;

    final Calendar cal = Calendar.getInstance();
    StringBuilder date = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_challenge);
        ButterKnife.bind(this);

        datumVandaag = (TextView) findViewById(R.id.datumvandaag);
        challengedescr = (TextView) findViewById(R.id.challengediscription);
        radioGroup = (RadioGroup) findViewById(R.id.radiogrp);
        bVoltooid = (Button) findViewById(R.id.bVoltooid);

        //TODO: Getting 3 daily challenges and progress out of DB
        option1 = (RadioButton) findViewById(R.id.option1);
        option2 = (RadioButton) findViewById(R.id.option2);
        option3 = (RadioButton) findViewById(R.id.option3);
        progress = (TextView) findViewById(R.id.progress);
        progress.setText("Dag 1");

        //Displaying todays date
        date.append(cal.get(Calendar.DAY_OF_MONTH) + "/");
        date.append(String.valueOf(cal.get(Calendar.MONTH) + 1) + "/");
        date.append(cal.get(Calendar.YEAR) + "/");
        datumVandaag.setText(date);

        bVoltooid.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                voltooiChallenge();
            }
        });

        // als progress = 1 of 21 => data vragen

        //showDataDialog();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                //TODO: Getting description out of DB

                switch (checkedId){
                    case R.id.option1:
                        challengedescr.setText("Maak eens een vegetarische snack in plaats van een zakje chips te openen.");
                        break;
                    case R.id.option2:
                        challengedescr.setText("Ga voor deze uitdaging eens eten bij een vegetarisch resaurant," +
                                "kies hier een lekker vegetarisch gerechtje en proberen erachter te komen welke ingredienten erin zitten," +
                                "misschien iets voor thuis? Maak ook een foto van je eten en deel het op instagram.");
                        break;
                    case R.id.option3:
                        challengedescr.setText("Nodig je vrienden uit om bij jou thuis te komen eten, maar vertel niet dat het vegetarisch zal zijn. " +
                                "Vraag na het eten naar hun mening!");
                        break;
                }

            }
        });

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_daily_challenge, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void voltooiChallenge(){
        int id = radioGroup.getCheckedRadioButtonId();
        RadioButton selected = (RadioButton) radioGroup.findViewById(id);
        String challenge = String.valueOf(selected.getText());
        String s = challenge.substring(0,challenge.length()-4);
        Context context = (Context) this;
        //Confirming completion challenge
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("De Challenge '" + s + "' is voltooid!")
                .setPositiveButton("Ok!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.show();
        if(!option1.isEnabled() && !option2.isEnabled() && !option3.isEnabled()){
            challengedescr.setText("Je hebt alle challenges voor vandaag voltooid!");
        } else {
            challengedescr.setText("");
        }
        selected.setEnabled(false);
        //TODO: write in DB.
    }

    //public void showDataDialog(){
       // PersonalDateDialog cdd=new PersonalDateDialog(this);
       // cdd.show();
    // }
    @OnClick(R.id.bVolgende)
    public void volgende(){
        startActivity(new Intent(this, KomenEtenStartActivity.class));
    }
}
