package com.example.tamas.evaapplication.KomenEten;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.w3c.dom.Text;
import java.io.Serializable;

public class Deelnemer extends LinearLayout{

    private String naam;
    private int score;
    private EditText voegPuntenToeEdit;
    private Button voegPuntenToe;
    private TextView verwijder;
    private TextView strNaam;
    private TextView strScore;

    public Deelnemer(Context context) {
        super(context);
        initialize();
    }

    public Deelnemer(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public Deelnemer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Deelnemer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }
    public void initialize(){
        strScore = new TextView(getContext());
        strNaam = new TextView(getContext());
        verwijder = new TextView(getContext());
        voegPuntenToe = new Button(getContext());
        voegPuntenToe.setText("Geef punten");
        voegPuntenToeEdit = new EditText(getContext());
        verwijder.setText("Verwijder");
        strScore.setVisibility(GONE);
        voegPuntenToeEdit.setVisibility(GONE);
        voegPuntenToeEdit.setHint("Voer punten in");
        voegPuntenToe.setVisibility(GONE);
        this.addView(strNaam);
        this.addView(strScore);
        this.addView(voegPuntenToeEdit);
        this.addView(voegPuntenToe);
        this.addView(verwijder);

        verwijder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                View row = (View) v.getParent();
                ViewGroup container = ((ViewGroup)row.getParent());
                container.removeView(row);
                container.invalidate();
            }
        });
        if(score == 0){
            voegPuntenToe.setVisibility(VISIBLE);
            voegPuntenToe.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(voegPuntenToeEdit.getVisibility() == VISIBLE){
                        setScore(Integer.parseInt(voegPuntenToeEdit.getText().toString()));
                        voegPuntenToeEdit.setVisibility(GONE);
                        voegPuntenToe.setVisibility(GONE);
                    }
                    else{
                        voegPuntenToeEdit.setVisibility(VISIBLE);
                        voegPuntenToe.setText("Vul in");
                    }
                }
            });
        }
    }

    public void setNaam(String n){
        naam = n;
        strNaam.setText(n);
    }
    public void setScore(int s){
        score = s;
        if(score > 0){
            strScore.setVisibility(VISIBLE);
            strScore.setText(Integer.toString(score));
            voegPuntenToe.setVisibility(GONE);
        }
    }
    public String getNaam() {
        return naam;
    }

    public int getScore() {
        return score;
    }
}
