package com.example.tamas.evaapplication.KomenEten;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.tamas.evaapplication.R;
import com.example.tamas.evaapplication.UserLocalStore;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class KomenEtenStartActivity extends AppCompatActivity {
    @Bind(R.id.btnNewKomenEtenGame)
    Button newGame;
    @Bind(R.id.btnOpenKomenEtenGame)
    Button openGame;

    //verplaatsen naar homescreen
    UserLocalStore userLocalStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komen_eten_start);
        ButterKnife.bind(this);
        userLocalStore = new UserLocalStore(this);
    }
    @OnClick(R.id.btnNewKomenEtenGame)
    public void newGame(View v) { startActivity(new Intent(this, KomenEtenNewGameActivity.class)); }
    @OnClick(R.id.btnOpenKomenEtenGame)
    public void openGame(View v){
        startActivity(new Intent(this, KomenEtenOpenActivity.class));
    }

    //verplaatsen naar homescreen
    @Override
    protected void onStart(){
        super.onStart();

        if(authenticate()){
            Log.i("true", "user is ingelogd");
        }
    }
    private boolean authenticate(){
        return userLocalStore.getUserLoggedIn();
    }
}
