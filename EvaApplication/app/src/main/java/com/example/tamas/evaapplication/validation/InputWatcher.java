package com.example.tamas.evaapplication.validation;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Thomas on 20/10/2015.
 */
public class InputWatcher {
    public final TextWatcher passWordWatcher(){
        TextWatcher result = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        return result;
    }
}
