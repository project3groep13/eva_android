package com.example.tamas.evaapplication.KomenEten;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.tamas.evaapplication.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

public class KomenEtenGameDetailActivity extends AppCompatActivity {
    @Bind(R.id.tblDeelnemersHuidigSpel)
    TableLayout deelnemersTable;
    @Bind(R.id.strTitelHuidigSpel)
    TextView titel;
    private String title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komen_eten_game_detail);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        titel = new TextView(this);
        titel.setText(intent.getStringExtra("titel"));
        vulTableMetDeelnemers();
    }
    public void vulTableMetDeelnemers(){
        Map<String, Integer> deelnemersMap;
        Intent intent = getIntent();
        deelnemersMap = (Map<String, Integer>) intent.getSerializableExtra("deelnemers");
        List<Deelnemer> deelnemers = new ArrayList<>();
        for(String key : deelnemersMap.keySet()){
            Deelnemer d = new Deelnemer(this);
            d.setNaam(key);
            d.setScore(deelnemersMap.get(key));
            deelnemers.add(d);
        }
        for(Deelnemer deelnemer : deelnemers){
            TableRow row = new TableRow(this);
            row.addView(deelnemer);
            deelnemersTable.addView(row);
        }
    }
}
