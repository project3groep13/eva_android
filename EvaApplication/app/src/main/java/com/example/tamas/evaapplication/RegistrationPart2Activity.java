package com.example.tamas.evaapplication;

import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.provider.MediaStore;
import android.renderscript.ScriptGroup;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.tamas.evaapplication.KomenEten.KomenEtenStartActivity;
import com.example.tamas.evaapplication.validation.InputValidatorHelper;

import java.util.Date;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationPart2Activity extends AppCompatActivity {
    @Bind(R.id.txtFullName)
    EditText fullName;
    @Bind(R.id.chkMale)
    RadioButton male;
    @Bind(R.id.chkFemale)
    RadioButton female;
    @Bind(R.id.txtdate)
    EditText dteDateOfBirth;
    @Bind(R.id.btnFinish)
    Button finish;
    //@Bind(R.id.skbLevel)
    //SeekBar sb;

    UserLocalStore userLocalStore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_part2);
        ButterKnife.bind(this);
        userLocalStore = new UserLocalStore(this);

        final EditText txtDate=(EditText)findViewById(R.id.txtdate);
        txtDate.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            public void onFocusChange(View view, boolean hasfocus){
                if(hasfocus){
                    PersonalDateDialog dialog = new PersonalDateDialog(view);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    dialog.show(ft, "DatePicker");
                    txtDate.clearFocus();
                }
            }
         });
    }

    @OnClick(R.id.btnFinish)
    public void finish(View v){
        InputValidatorHelper validator = new InputValidatorHelper();
        boolean allowSave = true;
        String date = dteDateOfBirth.getText().toString();
        String year = date.substring(date.lastIndexOf("-") + 1);
        String month = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        String day = date.substring(0, date.indexOf("-"));
        Date birthDate = new Date();
        birthDate.setYear(Integer.parseInt(year));
        birthDate.setMonth(Integer.parseInt(month));
        birthDate.setDate(Integer.parseInt(day));
        if(validator.isInFuture(birthDate)){
            dteDateOfBirth.setError("De datum mag niet in de toekomst liggen");
            allowSave = false;
        }
        if(!validator.isValidFullName(fullName.getText().toString())){
            fullName.setError("Dit is een verplicht veld");
            allowSave = false;
        }
        if(allowSave){
            Intent intent = getIntent();
            User user = new User();
            user.setEmail(intent.getStringExtra("email"));
            user.setPassword(intent.getStringExtra("password"));
            if(male.isChecked())
                user.setMale(true);
            else
                user.setMale(false);
            user.setBirthDate(birthDate);
            user.setFullname(fullName.getText().toString());
            userLocalStore.setUserLoggedIn(true);
            startActivity(new Intent(this, KomenEtenStartActivity.class));
        }

    }
    /*public void setUpSeekBar(){
        sb = (SeekBar) findViewById(R.id.seekbar);
        final TextView value = (TextView) findViewById(R.id.seekbarValue);
        sb.setProgress(0);
        sb.incrementProgressBy(1);
        sb.setMax(7);
        value.setText(String.valueOf(sb.getProgress()));

        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value.setText(String.valueOf(progress));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }*/
}
