package com.example.tamas.evaapplication;

import java.util.Date;

/**
 * Created by Thomas on 20/10/2015.
 */
public class User {
    String email, password, fullname;
    boolean male;
    Date birthDate;
    public User(){}
    public User(String email, String password, String fullname, boolean male, Date birthDate){
        this.email = email;
        this.password = password;
        this.fullname = fullname;
        this.male = male;
        this.birthDate = birthDate;
    }
    public User(String email, String password){
        this.email = email;
        this.password = password;
        this.male = false;
        this.birthDate = new Date();
        this.fullname = "";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
