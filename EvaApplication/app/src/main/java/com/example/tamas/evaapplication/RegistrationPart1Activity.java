package com.example.tamas.evaapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.tamas.evaapplication.validation.InputValidatorHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationPart1Activity extends AppCompatActivity {
    @Bind(R.id.txtEmailAddress)
    EditText emailAddress;
    @Bind(R.id.txtPassword)
    EditText password;
    @Bind(R.id.txtConfirmPassword)
    EditText confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_part1);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.btnNextRegistrationStep)
    public void next(View v) {
        InputValidatorHelper validator = new InputValidatorHelper();
        boolean allowSave = true;
        if (!validator.isValidEmail(emailAddress.getText().toString())) {
            //MOET NOG IN STRING.XML VOOR VERTALING
            emailAddress.setError("Je hebt een foutief emailadres ingevuld");
            allowSave = false;
        }
        if (!validator.isValidPassword(password.getText().toString(), false)){
            //MOET NOG IN STRING.XML VOOR VERTALING
            password.setError("Je hebt een foutief wachtwoord ingevuld");
            allowSave = false;
        }
        if(!password.getText().toString().equals(confirmPassword.getText().toString())){
            //MOET NOG IN STRING.XML VOOR VERTALING
            confirmPassword.setError("Komt niet overeen met het ingevulde wachtwoord");
            allowSave = false;
        }
        if(allowSave){
            Intent intent = new Intent(this, RegistrationPart2Activity.class);
            intent.putExtra("email", emailAddress.getText().toString());
            intent.putExtra("password", password.getText().toString());
            startActivity(intent);
        }

    }

}
