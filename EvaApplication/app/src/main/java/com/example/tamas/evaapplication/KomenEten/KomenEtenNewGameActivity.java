package com.example.tamas.evaapplication.KomenEten;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.tamas.evaapplication.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class KomenEtenNewGameActivity extends AppCompatActivity {
    @Bind(R.id.btnVoegNieuweDeelnemerToe)
    Button voegNieuweDeelnemerToe;
    @Bind(R.id.btnVoegDeelnemerToe)
    Button voegDeelnemerToe;
    @Bind(R.id.strKomenEtenAddTitel)
    EditText title;
    @Bind(R.id.strAddDeelnemer)
    EditText deelnemer;
    @Bind(R.id.addDeelnemer)
    LinearLayout addDeelnemerLayout;
    @Bind(R.id.layoutKomenEtenNewGame)
    LinearLayout home;
    @Bind(R.id.tblDeelnemers)
    TableLayout deelnemers;
    @Bind(R.id.btnSlaNieuwKomenEtenSpelOp)
    Button slaOp;

    TableRow tr;

    int aantalDeelnemers = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komen_eten_new_game);
        ButterKnife.bind(this);
        slaOp.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnVoegNieuweDeelnemerToe)
    public void toonLayout(View v){
        addDeelnemerLayout.setVisibility(View.VISIBLE);
        deelnemer.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(deelnemer, InputMethodManager.SHOW_IMPLICIT);
        voegNieuweDeelnemerToe.setVisibility(View.GONE);
    }
    @OnClick(R.id.btnVoegDeelnemerToe)
    public void voegDeelnemerToe(View v){
        String deelnemerString = deelnemer.getText().toString();
        tr = new TableRow(this);
        Deelnemer d = new Deelnemer(v.getContext());
        d.setNaam(deelnemerString);
        tr.addView(d);
        deelnemers.addView(tr);
        if(deelnemers.getChildCount() >= 2){
            slaOp.setVisibility(View.VISIBLE);
        }
        deelnemer.setText("");
        addDeelnemerLayout.setVisibility(View.GONE);
        voegNieuweDeelnemerToe.setVisibility(View.VISIBLE);
    }
    @OnClick(R.id.btnSlaNieuwKomenEtenSpelOp)
    public void slaOp(View v){
        Intent intent = new Intent(this, KomenEtenGameDetailActivity.class);
        intent.putExtra("titel", title.getText().toString());
        Map<String,Integer> deelnemerList = new HashMap<>();
        //HIER WEET IK NIET HOE IK EEN LIST VAN DEELNEMERS KAN DOORSTUREN NAAR DE INTENT
        //VIA PUTEXTRA KREEG IK EEN FOUT
        //OOK VIA SERIALIZABLE EN PARCABLE GEPROBEERT
        //DUS NU EEN MAP DOORSTUREN MET DE NAMEN VAN DE DEELNEMERS MET HUN SCORES
        //VRAGEN AAN MR BUYSSE OM UITLEG!
        for(int i = 0; i < deelnemers.getChildCount(); i++){
            TableRow r = (TableRow) deelnemers.getChildAt(i);
            Deelnemer d = (Deelnemer) r.getChildAt(0);
            deelnemerList.put(d.getNaam(), d.getScore());
        }
        intent.putExtra("deelnemers", (Serializable) deelnemerList);
        startActivity(intent);
    }
}
